import xlrd
import sys
import os

sys.path.append('../SqlBuilder')

from SqlBuilder import DataImport


class ExcelImport(object):
    def __init__(self,
                 filename: str,
                 database_name: str,
                 schema_name: str = '',
                 table_name: str = ''
                 ):
        if not table_name or not table_name.strip():
            self.base_table_name = os.path.basename(filename).split('.')[0]
        else:
            self.base_table_name = table_name

        self.schema_name = schema_name
        self.database_name = database_name

        self.workbook = xlrd.open_workbook(filename)

        self.imports = []

        self.__all_sheets_to_imports()

    def write_output_files(self):
        for imp in self.imports:
            imp.write_output_file()

    def get_all_sql(self):
        sql = []
        for imp in self.imports:
            sql.append(imp.generate_all_sql())

        return '\n'.join(sql)

    def get_table_names(self):
        names = []
        for imp in self.imports:
            names.append(imp.table_name)

        return names

    def __all_sheets_to_imports(self):
        for sheet in self.workbook.sheets():
            self.imports.append(self.__sheet_to_import(sheet))

    def __sheet_to_import(self, worksheet):
        data = [
            [worksheet.cell_value(r, col)
             for col in range(worksheet.ncols)]
            for r in range(worksheet.nrows)
        ]

        table_name = '{0}_{1}'.format(self.base_table_name, worksheet.name)

        return DataImport(database_name=self.database_name,
                          schema_name=self.schema_name,
                          table_name=table_name,
                          column_names=data[0],
                          rows=data[1:]
                          )

    def __get_table_names(self):
        """
        Description:
            Return a list of table names
            This import may have multiple tables since an Excel workbook can have multiple worksheets

        If is_include_sheet_names_in_table_name:
            return BaseTableName_SheetName
        If is_include_sheet_names_in_table_name == False:
            if one sheet:
                return BaseTableName
            else:
                return BaseTableName_N
                    Where N is the index of the sheet
        :return: []
        """
        table_names = []

        for sheet in self.workbook.sheet_names():
            table_names.append('{0}_{1}'.format(self.base_table_name, sheet))

        return table_names


def create_all_sql_in_dir():
    for file in os.listdir(os.path.curdir):
        split = os.path.splitext(file)
        file_ext = split[len(split) - 1]
        if not file_ext in ['.xlsx', '.xls']:
            continue

        ei = ExcelImport(file, 'tempdb')
        try:
            ei.write_output_files()
        except Exception as e:
            print('Failed to write output for file: {0}'.format(file))
            print(e)


if __name__ == '__main__':
    create_all_sql_in_dir()
